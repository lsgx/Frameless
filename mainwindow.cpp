#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QDesktopWidget>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    setWindowFlags(Qt::FramelessWindowHint);

    mLocation = this->geometry();
    mIsMax = false;
    mDrag = false;

    mZoom = false;

    ui->widgetTitle->installEventFilter(this);
    ui->btnMin->setIcon(QIcon(":/image/min.png"));
    ui->btnMax->setIcon(QIcon(":/image/max1.png"));
    ui->btnExit->setIcon(QIcon(":/image/exit.png"));

    //给缩放的图片label处理鼠标的按下、移动、弹起消息，进行缩放窗口功能
    ui->labelZoom->installEventFilter(this);
    ui->labelZoom->setCursor(Qt::SizeFDiagCursor);

    setStyleSheet("QMainWindow{color:#E8E8E8;background:#43CD80;}");

    //另外在设计器内查看widgetTitle样式，内有标题栏各个子控件的样式设置
}

MainWindow::~MainWindow()
{
    delete ui;
}

bool MainWindow::eventFilter(QObject *obj, QEvent *e)
{
    if (obj == ui->widgetTitle)
    {
        if(e->type() == QEvent::MouseButtonDblClick)
        {
            on_btnMax_clicked();
            return true;
        }
    }
    else if (obj == ui->labelZoom)
    {
        //实现拖动右下角缩放窗口
        if(e->type() == QEvent::MouseButtonPress)
        {
            QMouseEvent *event = (QMouseEvent *)e;
            if (event->buttons() & Qt::LeftButton)
            {
                if(mIsMax)
                {
                    //已最大化，就不让再拖动
                    return true;
                }
                mZoom = true;
                mZoomLocation = geometry();
                mZoomPos = event->globalPos();
                return true;
            }
        }
        else if(e->type() == QEvent::MouseMove)
        {
            QMouseEvent *event = (QMouseEvent *)e;
            if (mZoom && (event->buttons() & Qt::LeftButton))
            {
                int dx = event->globalPos().x() - mZoomPos.x();
                int dy = event->globalPos().y() - mZoomPos.y();
                QRect rc = mZoomLocation;
                rc.setRight(rc.right() + dx);
                rc.setBottom(rc.bottom() + dy);
                setGeometry(rc);
                update();
                return true;
            }
        }
        else if(e->type() == QEvent::MouseButtonRelease)
        {
            mZoom = false;
            return true;
        }
    }
    return QObject::eventFilter(obj, e);
}

void MainWindow::mousePressEvent(QMouseEvent *e)//鼠标按下事件
{
    if (e->button() == Qt::LeftButton)
    {
        mDrag = true;
        mDragPos = e->globalPos() - pos();
        e->accept();
    }
}

void MainWindow::mouseMoveEvent(QMouseEvent *e)//鼠标移动事件
{
    if (mDrag && (e->buttons() && Qt::LeftButton))
    {
        move(e->globalPos() - mDragPos);
        e->accept();
    }
}

void MainWindow::mouseReleaseEvent(QMouseEvent *e)//鼠标释放事件
{
    mDrag = false;
}

void MainWindow::on_btnMin_clicked()
{
    showMinimized();
}

void MainWindow::on_btnMax_clicked()
{
    if (mIsMax)
    {
        setGeometry(mLocation);
        ui->btnMax->setIcon(QIcon(":/image/max1.png"));
        ui->btnMax->setToolTip(QStringLiteral("最大化"));
    }
    else
    {
        mLocation = geometry();
        setGeometry(qApp->desktop()->availableGeometry());
        ui->btnMax->setIcon(QIcon(":/image/max2.png"));
        ui->btnMax->setToolTip(QStringLiteral("还原"));
    }
    mIsMax = !mIsMax;
}

void MainWindow::on_btnExit_clicked()
{
    qApp->exit();
}
